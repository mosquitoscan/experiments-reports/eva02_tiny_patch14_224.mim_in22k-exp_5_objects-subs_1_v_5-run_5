
Model info for eva02_tiny_patch14_224.mim_in22k
===============================================


Sequential (Input shape: 64 x 3 x 224 x 224)
============================================================================
Layer (type)         Output Shape         Param #    Trainable 
============================================================================
                     64 x 192 x 16 x 16  
Conv2d                                    113088     True      
Identity                                                       
Dropout                                                        
LayerNorm                                 384        True      
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 257 x 1024     
Linear                                    197632     True      
SiLU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 257 x 192      
Linear                                    98496      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 257 x 1024     
Linear                                    197632     True      
SiLU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 257 x 192      
Linear                                    98496      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 257 x 1024     
Linear                                    197632     True      
SiLU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 257 x 192      
Linear                                    98496      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 257 x 1024     
Linear                                    197632     True      
SiLU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 257 x 192      
Linear                                    98496      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 257 x 1024     
Linear                                    197632     True      
SiLU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 257 x 192      
Linear                                    98496      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 257 x 1024     
Linear                                    197632     True      
SiLU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 257 x 192      
Linear                                    98496      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 257 x 1024     
Linear                                    197632     True      
SiLU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 257 x 192      
Linear                                    98496      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 257 x 1024     
Linear                                    197632     True      
SiLU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 257 x 192      
Linear                                    98496      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 257 x 1024     
Linear                                    197632     True      
SiLU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 257 x 192      
Linear                                    98496      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 257 x 1024     
Linear                                    197632     True      
SiLU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 257 x 192      
Linear                                    98496      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 257 x 1024     
Linear                                    197632     True      
SiLU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 257 x 192      
Linear                                    98496      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
Identity                                                       
Linear                                    37056      True      
Dropout                                                        
Identity                                                       
LayerNorm                                 384        True      
____________________________________________________________________________
                     64 x 257 x 1024     
Linear                                    197632     True      
SiLU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 257 x 192      
Linear                                    98496      True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm                                 384        True      
Dropout                                                        
Identity                                                       
BatchNorm1d                               384        True      
Dropout                                                        
____________________________________________________________________________
                     64 x 512            
Linear                                    98304      True      
ReLU                                                           
BatchNorm1d                               1024       True      
Dropout                                                        
____________________________________________________________________________
                     64 x 12             
Linear                                    6144       True      
____________________________________________________________________________

Total params: 4,226,752
Total trainable params: 4,226,752
Total non-trainable params: 0

Optimizer used: <function Adam at 0x7dbe738e8040>
Loss function: FlattenedLoss of CrossEntropyLoss()

Callbacks:
  - TrainEvalCallback
  - CastToTensor
  - Recorder
  - ProgressCallback